#include "cv.h"
#include "highgui.h"

using namespace cv;

void generateLookup()
{
	Mat image(512, 512, CV_8UC3);
	Vec3b intensity;

	// TODO: There is a small aliasing problem.
    for (int by = 0; by < 8; by++) {
        for (int bx = 0; bx < 8; bx++) {
            for (int g = 0; g < 64; g++) {
                for (int r = 0; r < 64; r++) {
                	uchar red = (int)(r * 255.0 / 63.0 + 0.5);
					uchar green = (int)(g * 255.0 / 63.0 + 0.5);
					uchar blue = (int)((bx + by * 8.0) * 255.0 / 63.0 + 0.5);

					image.at<Vec3b>(g + by * 64, r + bx * 64) = Vec3b(blue,green,red);

					//Do stuff with blue, green, and red


					/*
                    image.setPixel(r + bx * 64, g + by * 64, qRgb((int)(r * 255.0 / 63.0 + 0.5),
                                                                  (int)(g * 255.0 / 63.0 + 0.5),
                                                                  (int)((bx + by * 8.0) * 255.0 / 63.0 + 0.5)));
                    */
                }
            }
        }
    }

    imshow("Lookup Image", image );
    imwrite( "gen.png", image );
    cvWaitKey(0);
}

int main(int argc, char **argv)
{
	/*
	IplImage *img = cvLoadImage(argv[1]);
	cvNamedWindow(argv[0], 0);
	cvShowImage(argv[0], img);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvDestroyWindow(argv[0]);
	*/

	generateLookup();

	return(0);
}